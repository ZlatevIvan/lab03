// Gulp.js configuration


// modules
var gulp = require('gulp');
var browserSync = require('browser-sync').create();
var newer = require('gulp-newer');                  // https://www.npmjs.com/package/gulp-newer
var imagemin = require('gulp-imagemin');            // https://www.npmjs.com/package/gulp-imagemin
var htmlclean = require('gulp-htmlclean');          // https://www.npmjs.com/package/gulp-htmlclean
var concat = require('gulp-concat');                // https://www.npmjs.com/package/gulp-concat
var stripdebug = require('gulp-strip-debug');       // https://www.npmjs.com/package/gulp-strip-debug
var uglify = require('gulp-uglify');                // https://www.npmjs.com/package/gulp-uglify
var sourcemaps = require('gulp-sourcemaps');        // https://www.npmjs.com/package/gulp-sourcemaps
var sass = require('gulp-sass');                    // https://www.npmjs.com/package/gulp-sass
var postcss = require('gulp-postcss');              // https://github.com/postcss/gulp-postcss
var autoprefixer = require('autoprefixer');         //https://www.npmjs.com/package/autoprefixer
var cssnano = require('cssnano');                   // https://www.npmjs.com/package/cssnano



// Dossiers du projet
var folder = {
    src: 'src/',
    dist: 'dist/'
};


// Commande pour copier le normalize.css du dossier node-modules vers le dossier dist
gulp.task('copy:normalize', function() {
    gulp.src('node_modules/normalize.css/normalize.css')
    .pipe(gulp.dest(folder.dist +'/css'))
});



// Processus d’optimisation des images
gulp.task('images', function() {
    var out = folder.dist + 'images/';

    return gulp.src(folder.src + 'images/**/*')     //Récupère tous les fichiers du dossier et des sous-dossiers.
        .pipe(newer(out))                           //Permets de traiter seulement les nouveaux fichiers ou ceux qui ont été modifiés.
        .pipe(imagemin({ optimizationLevel: 5 }))   //Optimisation des fichiers images au format PNG, JPEG, GIF et SVG.
        .pipe(gulp.dest(out));                      //Copie tous les fichiers optimisés vers la destination.
});


// Processus d’optimisation du HTML
gulp.task('html', ['images'], function() {
    var out = folder.dist;

    return gulp.src(folder.src + '/**/*.html')           //Récupère tous les fichiers du dossier et des sous-dossiers.
        .pipe(newer(out))                           //Permets de traiter seulement les nouveaux fichiers ou ceux qui ont été modifiés.
        .pipe(htmlclean())                          //Reformate le html sur une seule ligne
        .pipe(gulp.dest(out));                      //Copie tous les fichiers optimisés vers la destination.


});


// Processus d’optimisation du JavaScript
gulp.task('js', function() {
    var out = folder.dist;

    return gulp.src(folder.src + 'js/**/*')         //Récupère tous les fichiers du dossier et des sous-dossiers.
        .pipe(sourcemaps.init())                    //Permets de retrouver la ligne problématique dans le fichier original.
        //TODO enlever le commentaire de stripdebug avant de mettre en ligne
        //.pipe(stripdebug())                         //Supprime tous les commentaires et les lignes de « débogage »
        .pipe(uglify())                             //Reformate le script sur une seule ligne
        .pipe(sourcemaps.write())                   //Permets de retrouver la ligne problématique dans le fichier original.
        .pipe(gulp.dest(out + 'js/'));              //Copie tous les fichiers optimisés vers la destination.
});


// Processus d’optimisation du CSS
gulp.task('css', ['images'], function() {

    var postCssOpts = [
        autoprefixer({ browsers: ['last 2 versions', '> 2%'] }),
        cssnano
    ];

    return gulp.src(folder.src + 'scss/main.scss')
        .pipe(sourcemaps.init())                    //Permets de retrouver la ligne problématique dans le fichier original.
        .pipe(sass({                                //Fait la compilation des fichiers SASS
            outputStyle: 'nested',
            imagePath: 'images/',
            precision: 3,
            errLogToConsole: true
        }))
        .pipe(postcss(postCssOpts))                 //Permet de faire des actions sur le css comme l'autoprefixeur et la compression du code
        .pipe(sourcemaps.write())                   //Permets de retrouver la ligne problématique dans le fichier original.
        .pipe(gulp.dest(folder.dist + 'css/'));

});



// Processus pour exécuter chaque tâche dans l'ordre
gulp.task('run', ['html', 'css', 'js', 'copy:normalize']);


// Processus qui vérifie s'il y a eu un changement dans le dossier et exécute le processus qui s'y rattache
gulp.task('watch', function() {

    // image changes
    gulp.watch(folder.src + 'images/**/*', ['images']);

    // html changes
    gulp.watch(folder.src + '**/*', ['html']);

    // javascript changes
    gulp.watch(folder.src + 'js/**/*', ['js']);

    // css changes
    gulp.watch(folder.src + 'scss/**/*', ['css']);

});


//Processus qui lance le seurveur Web local et qui recharge la page lorsqu'il y a un changement avec les fichier CSS, HTML et JS
gulp.task('serveur', function () {

    browserSync.init({
        port: 3000,
        server: "./dist/",
        ghostMode: false,
        notify: false
    });

    //gulp.watch('**/*').on('change', browserSync.reload);
    gulp.watch('**/*.css').on('change', browserSync.reload);
    gulp.watch('**/*.html').on('change', browserSync.reload);
    gulp.watch('js/**/*.js').on('change', browserSync.reload);

});

//Si vous avez un problème avec le départ du serveur sur le port 3000, vérifier s’il y a déjà des processus qui utilisent le port 3000 avec la commande au terminal : lsof -i:3000
//Si c’est le cas, supprimer le processus avec la commande « kill -9 $pid ». Remplacer « $pid » par le numéro récupéré avec la commande lsof.



// Processus par défaut
gulp.task('default', ['serveur', 'run', 'watch']);